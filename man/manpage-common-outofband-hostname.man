.TP
\fB\-h\fR \fIIPMIHOST\FR, \fB\-\-hostname\fR=\fIIPMIHOST\fR
Specify the remote host to communicate with.
