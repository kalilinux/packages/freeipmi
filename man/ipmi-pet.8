.TH IPMI-PET 8 "2012-06-28" "IPMI-PET version 1.1.6" "System Commands"
.SH "NAME"
IPMI \- IPMI Platform Event Trap Interpreter
.SH "SYNOPSIS"
.B ipmi-pet
[\fIOPTION\fR...] [SPECIFIC TRAP] [VARIABLE BINDING HEX BYTES ...]
.SH "DESCRIPTION"
.B Ipmi-pet
interprets hex bytes from a platform event trap (PET) and outputs a
string representing its contents. Hex values may be input on the
command line, a file via the \fB\-\-file\fR option, or via stdin if
neither of the previous are specified.

.B Ipmi-pet
is commonly used in conjunction with an SNMP trap daemon to intrepret
the results from an IPMI PET trap captured by the daemon. While
.B ipmi-pet
could be called directly from such a daemon, typically a script is called
to parse the SNMP daemon's output and convert it into a form that can be input
into
.B ipmi-pet.
On some systems, you may wish to also send a PET acknowledge to a
remote system to inform it the trap was received and parsed. One can
be sent using the \fB\-\-pet-acknowledge\fR option.

While an IPMI session is not required to interpret a PET, data from
the sensor data repository (SDR) is required to properly interpret
sensor names and other information in the PET. IPMI session
configuration below, such as driver, hostname, username, etc. should
be configured to load the SDR of the host where the trap originated.
If this is difficult to perform, it may be wise to cache and load a
specific SDR cache using the \fB\-\-sdr\-cache\-file\fR option.
If the SDR is difficult to obtain, the \fB\-\-ignore\-sdr\-cache\fR
option can be specified so that an SDR will not be loaded, and an IPMI
session will not be required. The PET will be interpreted as best as
possible given no SDR. The \fB\-\-ignore\-sdr\-cache\fR option may
affect other options such as \fB\-\-interpret\-oem\-data\fR too. Some
options, such as \fB\-\-manufacturer\-id\fR and \fB\-\-product\-id\fR
may alleviate some of these issues.

If the SNMP daemon does not output a SNMPv1 \fIspecific trap\fR on its
own, it is typically output as the last element of the OID in SNMPv2.
If for some reason a \fIspecific trap\fR cannot be determined, the
value of \fINA\fR may be input for the \fIspecific trap\fR to indicate
it is not available.
.B Ipmi-pet
will output as much as possible based on the variable bindings
information. Some of the specific trap information may be obtained
via SDR information.
.LP
Listed below are general IPMI options, tool specific options, trouble
shooting information, workaround information, examples, and known
issues. For a general introduction to FreeIPMI please see
.B freeipmi(7).
.SH "GENERAL OPTIONS"
The following options are general options for configuring IPMI
communication and executing general tool commands.
.TP
\fB\-D\fR \fIIPMIDRIVER\fR, \fB\-\-driver\-type\fR=\fIIPMIDRIVER\fR
Specify the driver type to use instead of doing an auto selection.
The currently available outofband drivers are LAN and LAN_2_0, which
perform IPMI 1.5 and IPMI 2.0 respectively. The currently available
inband drivers are KCS, SSIF, OPENIPMI, and SUNBMC.
.TP
\fB\-\-disable\-auto\-probe\fR
Do not probe in-band IPMI devices for default settings.
.TP
\fB\-\-driver\-address\fR=\fIDRIVER-ADDRESS\fR
Specify the in-band driver address to be used instead of the probed
value. \fIDRIVER-ADDRESS\fR should be prefixed with "0x" for a hex
value and '0' for an octal value.
.TP
\fB\-\-driver\-device\fR=\fIDEVICE\fR
Specify the in-band driver device path to be used instead of the
probed path.
.TP
\fB\-\-register\-spacing\fR=\fIREGISTER-SPACING\fR
Specify the in-band driver register spacing instead of the probed
value. Argument is in bytes (i.e. 32bit register spacing = 4)
.TP
\fB\-h\fR \fIIPMIHOST\FR, \fB\-\-hostname\fR=\fIIPMIHOST\fR
Specify the remote host to communicate with.
.TP
\fB\-u\fR \fIUSERNAME\fR, \fB\-\-username\fR=\fIUSERNAME\fR
Specify the username to use when authenticating with the remote host.
If not specified, a null (i.e. anonymous) username is assumed. The
user must have atleast OPERATOR privileges in order for this tool to
operate fully.
.TP
\fB\-p\fR \fIPASSWORD\fR, \fB\-\-password\fR=\fIPASSWORD\fR
Specify the password to use when authenticationg with the remote host.
If not specified, a null password is assumed. Maximum password length
is 16 for IPMI 1.5 and 20 for IPMI 2.0.
.TP
\fB\-P\fR, \fB\-\-password-prompt\fR
Prompt for password to avoid possibility of listing
it in process lists.
.TP
\fB\-k\fR \fIK_G\fR, \fB\-\-k-g\fR=\fIK_G\fR
Specify the K_g BMC key to use when authenticating with the remote
host for IPMI 2.0. If not specified, a null key is assumed. To input
the key in hexadecimal form, prefix the string with '0x'. E.g., the
key 'abc' can be entered with the either the string 'abc' or the
string '0x616263'
.TP
\fB\-K\fR, \fB\-\-k-g-prompt\fR
Prompt for k-g to avoid possibility of listing it in process lists.
.TP
\fB\-\-session-timeout\fR=\fIMILLISECONDS\fR
Specify the session timeout in milliseconds. Defaults to 20000
milliseconds (20 seconds) if not specified.
.TP
\fB\-\-retransmission-timeout\fR=\fIMILLISECONDS\fR
Specify the packet retransmission timeout in milliseconds. Defaults
to 1000 milliseconds (1 second) if not specified. The retransmission
timeout cannot be larger than the session timeout.
.TP
\fB\-a\fR \fIAUTHENTICATION\-TYPE\fR, \fB\-\-authentication\-type\fR=\fIAUTHENTICATION\-TYPE\fR
Specify the IPMI 1.5 authentication type to use. The currently
available authentication types are NONE, STRAIGHT_PASSWORD_KEY, MD2,
and MD5. Defaults to MD5 if not specified.
.TP
\fB\-I\fR \fICIPHER-SUITE-ID\fR, \fB\-\-cipher\-suite-id\fR=\fICIPHER-SUITE-ID\fR
Specify the IPMI 2.0 cipher suite ID to use. The Cipher Suite ID
identifies a set of authentication, integrity, and confidentiality
algorithms to use for IPMI 2.0 communication. The authentication
algorithm identifies the algorithm to use for session setup, the
integrity algorithm identifies the algorithm to use for session packet
signatures, and the confidentiality algorithm identifies the algorithm
to use for payload encryption. Defaults to cipher suite ID 3 if not
specified. The following cipher suite ids are currently supported:
.sp
0 - Authentication Algorithm = None; Integrity Algorithm = None; Confidentiality Algorithm = None
.sp
1 - Authentication Algorithm = HMAC-SHA1; Integrity Algorithm = None; Confidentiality Algorithm = None
.sp
2 - Authentication Algorithm = HMAC-SHA1; Integrity Algorithm = HMAC-SHA1-96; Confidentiality Algorithm = None
.sp
3 - Authentication Algorithm = HMAC-SHA1; Integrity Algorithm = HMAC-SHA1-96; Confidentiality Algorithm = AES-CBC-128
.\" .sp
.\" 4 - Authentication Algorithm = HMAC-SHA1; Integrity Algorithm = HMAC-SHA1-96; Confidentiality Algorithm = xRC4-128
.\" .sp
.\" 5 - Authentication Algorithm = HMAC-SHA1; Integrity Algorithm = HMAC-SHA1-96; Confidentiality Algorithm = xRC4-40
.sp
6 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = None; Confidentiality Algorithm = None
.sp
7 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = HMAC-MD5-128; Confidentiality Algorithm = None
.sp
8 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = HMAC-MD5-128; Confidentiality Algorithm = AES-CBC-128
.\" .sp
.\" 9 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = HMAC-MD5-128; Confidentiality Algorithm = xRC4-128
.\" .sp
.\" 10 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = HMAC-MD5-128; Confidentiality Algorithm = xRC4-40
.sp
11 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = MD5-128; Confidentiality Algorithm = None
.sp
12 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = MD5-128; Confidentiality Algorithm = AES-CBC-128
.\" .sp
.\" 13 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = MD5-128; Confidentiality Algorithm = xRC4-128
.\" .sp
.\" 14 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = MD5-128; Confidentiality Algorithm = xRC4-40
.\" XXX GUESS
.\" .sp
.\" 15 - Authentication Algorithm = HMAC-SHA256; Integrity Algorithm = None; Confidentiality Algorithm = None
.\" XXX GUESS
.\" .sp
.\" 16 - Authentication Algorithm = HMAC-SHA256; Integrity Algorithm = HMAC_SHA256_128; Confidentiality Algorithm = None
.sp
17 - Authentication Algorithm = HMAC-SHA256; Integrity Algorithm = HMAC_SHA256_128; Confidentiality Algorithm = AES-CBC-128
.\" XXX GUESS
.\" .sp
.\" 18 - Authentication Algorithm = HMAC-SHA256; Integrity Algorithm = HMAC_SHA256_128; Confidentiality Algorithm = xRC4-128
.\" XXX GUESS
.\" .sp
.\" 19 - Authentication Algorithm = HMAC-SHA256; Integrity Algorithm = HMAC_SHA256_128; Confidentiality Algorithm = xRC4-40
.TP
\fB\-l\fR \fIPRIVILEGE\-LEVEL\fR, \fB\-\-privilege\-level\fR=\fIPRIVILEGE\-LEVEL\fR
Specify the privilege level to be used. The currently available
privilege levels are USER, OPERATOR, and ADMIN. Defaults to OPERATOR
if not specified.
.TP
\fB\-\-config\-file\fR=\fIFILE\fR
Specify an alternate configuration file.
.TP
\fB\-W\fR \fIWORKAROUNDS\fR, \fB\-\-workaround\-flags\fR=\fIWORKAROUNDS\fR
Specify workarounds to vendor compliance issues. Multiple workarounds
can be specified separated by commas. A special command line flag of
"none", will indicate no workarounds (may be useful for overriding
configured defaults). See WORKAROUNDS below for a list of available
workarounds.
.TP
\fB\-\-debug\fR
Turn on debugging.
.TP
\fB\-?\fR, \fB\-\-help\fR
Output a help list and exit.
.TP
\fB\-\-usage\fR
Output a usage message and exit.
.TP
\fB\-V\fR, \fB\-\-version\fR
Output the program version and exit.
.SH "IPMI-PET OPTIONS"
The following options are specific to
.B Ipmi-pet.
.TP
\fB\-v\fR
Output verbose output. This option will output event direction and
OEM custom messages from the trap.
.TP
\fB\-vv\fR
Output very verbose output. This option will output additional
information available in the trap, such as GUID, manufacturer ID,
and system ID.
.TP
\fB\-vvv\fR
Output very very verbose output. This option will output additional
information than verbose output. Most notably it will output
additional hex codes to given information on ambiguous events. For
example, it will output Generator ID hex codes for sensors without
names.
.TP
\fB\-\-pet-acknowledge\fR
Send PET acknowledge using inputted trap data instead of outputting
data. In some circumstances, this may be useful to inform a remote
system that a trap was received and parsed. If specified, a hostname
must be specified via \fI\-h\fR or \fI\-\-hostname\fR to inform
.B ipmi-pet
where to send the acknowledge to. When this option is specified, the
SDR cache is not loaded and is not required.
.TP
\fB\-\-file\fR=\fICMD\-FILE\fR
Specify a file to read PET specific trap and variable bindings hex
from instead of command line.
.TP
\fB\-\-output\-event\-severity\fR
Output event severity in output. This will add an additional output
of an event severity. The outputs may be Monitor, Information, OK,
Non-critical condition, Critical condition, or Non-recoverable
condition. This differs from the output of
\fB\-\-output\-event\-state\fR, as event severity is not interpreted,
it is a value reported in the SNMP trap. However, not all events may
report a severity, or some manufacturers may not support the report of
a severity. Event severity will automatically be output under
verbose output.
.TP
\fB\-\-output\-event\-state\fR
Output event state in output. This will add an additional output
reporting if an event should be viewed as NOMINAL, WARNING, or
CRITICAL. This differs from the output of
\fB\-\-output\-event\-severity\fR, as this output is an interpreted
value that will be interpreted identically to the
\fB\-\-output\-event\-state\fR output in
.B ipmi-sel(8).
As long as an event interpretation is supported, all events will have
outputted state. The event state is an interpreted value based on the
configuration file /usr/local/etc/freeipmi//freeipmi_interpret_sel.conf and the event
direction. See
.B freeipmi_interpret_sel.conf(5)
for more information.
.TP
\fB\-\-event\-state\-config\-file\fR=\fIFILE\fR
Specify an alternate event state configuration file. Option ignored
if \fB\-\-output\-event\-state\fR not specified.
.TP
\fB\-\-manufacturer\-id\fR=\fINUMBER\FR
Specify a specific manufacturer id to assume. Useful if you wish to
specify \fB\-\-interpret\-oem\-data\fR, but the manufacturer id cannot
be determined by IPMI access or is not available in the SNMP trap.
The manufacturer id of a motherboard can be determined with
.B bmc-info(8).
If this option is specified, so must \fB\-\-product\-id\fR.
.TP
\fB\-\-product\-id\fR=\fINUMBER\FR
Specify a specific product id to assume. Useful if you wish to
specify \fB\-\-interpret\-oem\-data\fR, but the product id cannot
be determined by IPMI access or is not available in the SNMP trap.
The product id of a motherboard can be determined with
.B bmc-info(8).
If this option is specified, so must \fB\-\-manufacturer\-id\fR.
.TP
\fB\-\-interpret\-oem\-data\fR
Attempt to interpret OEM data, such as event data, sensor readings, or
general extra info, etc. If an OEM interpretation is not available,
the default output will be generated. Correctness of OEM
interpretations cannot be guaranteed due to potential changes OEM
vendors may make in products, firmware, etc. See OEM INTERPRETATION
below for confirmed supported motherboard interpretations.
.TP
\fB\-\-entity\-sensor\-names\fR
Output sensor names prefixed with their entity id and instance number
when appropriate. This may be necessary on some motherboards to help
identify what sensors are referencing. For example, a motherboard may
have multiple sensors named 'TEMP'. The entity id and instance number
may help clarify which sensor refers to "Processor 1" vs. "Processor
2".
.TP
\fB\-\-no\-sensor\-type\-output\fR
Do not show sensor type output for each entry. On many systems, the
sensor type is redundant to the name of the sensor. This can
especially be true if \fB\-\-entity\-sensor\-names\fR is specified.
If the sensor name is sufficient, or if the sensor type is of no
interest to the user, this option can be specified to condense output.
.TP
\fB\-\-comma\-separated\-output
Output fields in comma separated format.
.TP
\fB\-\-no\-header\-output
Do not output column headers. May be useful in scripting.
.TP
\fB\-\-non\-abbreviated\-units\fR
Output non-abbreviated units (e.g. 'Amps' instead of 'A'). May aid in
disambiguation of units (e.g. 'C' for Celsius or Coulombs).
.SH "SDR CACHE OPTIONS"
This tool requires access to the sensor data repository (SDR) cache
for general operation. By default, SDR data will be downloaded and
cached on the local machine. The following options apply to the SDR
cache.
.TP
\fB\-f\fR, \fB\-\-flush\-cache\fR
Flush a cached version of the sensor data repository (SDR) cache. The
SDR is typically cached for faster subsequent access. However, it may
need to be flushed and re-generated if the SDR has been updated on a
system.
.TP
\fB\-Q\fR, \fB\-\-\quiet\-cache\fR
Do not output information about cache creation/deletion. May be
useful in scripting.
.TP
\fB\-\-sdr\-cache\-directory\fR=\fIDIRECTORY\fR
Specify an alternate directory for sensor data repository (SDR) caches
to be stored or read from. Defaults to the home directory if not
specified.
.TP
\fB\-\-sdr\-cache\-file\fR=\fIFILE\fR
Specify a specific sensor data repository (SDR) cache file to be
stored or read from.
.TP
\fB\-\-sdr-cache-recreate\fR
If the SDR cache is out of date or invalid, automatically recreate the
sensor data repository (SDR) cache. This option may be useful for
scripting purposes.
.TP
\fB\-\-ignore\-sdr\-cache\fR
Ignore SDR cache related processing. May lead to incomplete or less
useful information being output, however it will allow functionality
for systems without SDRs or when the correct SDR cannot be loaded.
.SH "GENERAL TROUBLESHOOTING"
Most often, IPMI problems are due to configuration problems.
.LP
IPMI over LAN problems involve a misconfiguration of the remote
machine's BMC.  Double check to make sure the following are configured
properly in the remote machine's BMC: IP address, MAC address, subnet
mask, username, user enablement, user privilege, password, LAN
privilege, LAN enablement, and allowed authentication type(s). For
IPMI 2.0 connections, double check to make sure the cipher suite
privilege(s) and K_g key are configured properly. The
.B bmc-config(8)
tool can be used to check and/or change these configuration
settings.
.LP
Inband IPMI problems are typically caused by improperly configured
drivers or non-standard BMCs.
.LP
In addition to the troubleshooting tips below, please see WORKAROUNDS
below to also if there are any vendor specific bugs that have been
discovered and worked around.
.LP
Listed below are many of the common issues for error messages.
For additional support, please e-mail the <freeipmi\-users@gnu.org>
mailing list.
.LP
"username invalid" - The username entered (or a NULL username if none
was entered) is not available on the remote machine. It may also be
possible the remote BMC's username configuration is incorrect.
.LP
"password invalid" - The password entered (or a NULL password if none
was entered) is not correct. It may also be possible the password for
the user is not correctly configured on the remote BMC.
.LP
"password verification timeout" - Password verification has timed out.
A "password invalid" error (described above) or a generic "session
timeout" (described below) occurred.  During this point in the
protocol it cannot be differentiated which occurred.
.LP
"k_g invalid" - The K_g key entered (or a NULL K_g key if none was
entered) is not correct. It may also be possible the K_g key is not
correctly configured on the remote BMC.
.LP
"privilege level insufficient" - An IPMI command requires a higher
user privilege than the one authenticated with. Please try to
authenticate with a higher privilege. This may require authenticating
to a different user which has a higher maximum privilege.
.LP
"privilege level cannot be obtained for this user" - The privilege
level you are attempting to authenticate with is higher than the
maximum allowed for this user. Please try again with a lower
privilege. It may also be possible the maximum privilege level
allowed for a user is not configured properly on the remote BMC.
.LP
"authentication type unavailable for attempted privilege level" - The
authentication type you wish to authenticate with is not available for
this privilege level. Please try again with an alternate
authentication type or alternate privilege level. It may also be
possible the available authentication types you can authenticate with
are not correctly configured on the remote BMC.
.LP
"cipher suite id unavailable" - The cipher suite id you wish to
authenticate with is not available on the remote BMC. Please try
again with an alternate cipher suite id. It may also be possible the
available cipher suite ids are not correctly configured on the remote
BMC.
.LP
"ipmi 2.0 unavailable" - IPMI 2.0 was not discovered on the remote
machine. Please try to use IPMI 1.5 instead.
.LP
"connection timeout" - Initial IPMI communication failed. A number of
potential errors are possible, including an invalid hostname
specified, an IPMI IP address cannot be resolved, IPMI is not enabled
on the remote server, the network connection is bad, etc. Please
verify configuration and connectivity.
.LP
"session timeout" - The IPMI session has timed out. Please reconnect.
If this error occurs often, you may wish to increase the
retransmission timeout. Some remote BMCs are considerably slower than
others.
.LP
"device not found" - The specified device could not be found. Please
check configuration or inputs and try again.
.LP
"driver timeout" - Communication with the driver or device has timed
out. Please try again.
.LP
"message timeout" - Communication with the driver or device has timed
out. Please try again.
.LP
"BMC busy" - The BMC is currently busy. It may be processing
information or have too many simultaneous sessions to manage. Please
wait and try again.
.LP
"could not find inband device" - An inband device could not be found.
Please check configuration or specify specific device or driver on the
command line.
.LP
"driver timeout" - The inband driver has timed out communicating to
the local BMC or service processor. The BMC or service processor may
be busy or (worst case) possibly non-functioning.
.SH "WORKAROUNDS"
With so many different vendors implementing their own IPMI solutions,
different vendors may implement their IPMI protocols incorrectly. The
following describes a number of workarounds currently available to
handle discovered compliance issues. When possible, workarounds have
been implemented so they will be transparent to the user. However,
some will require the user to specify a workaround be used via the -W
option.
.LP
The hardware listed below may only indicate the hardware that a
problem was discovered on. Newer versions of hardware may fix the
problems indicated below. Similar machines from vendors may or may
not exhibit the same problems. Different vendors may license their
firmware from the same IPMI firmware developer, so it may be
worthwhile to try workarounds listed below even if your motherboard is
not listed.
.LP
If you believe your hardware has an additional compliance issue that
needs a workaround to be implemented, please contact the FreeIPMI
maintainers on <freeipmi\-users@gnu.org> or <freeipmi\-devel@gnu.org>.
.LP
\fIassumeio\fR - This workaround flag will assume inband interfaces
communicate with system I/O rather than being memory-mapped. This
will work around systems that report invalid base addresses. Those
hitting this issue may see "device not supported" or "could not find
inband device" errors.  Issue observed on HP ProLiant DL145 G1.
.LP
\fIspinpoll\fR - This workaround flag will inform some inband drivers
(most notably the KCS driver) to spin while polling rather than
putting the process to sleep. This may significantly improve the wall
clock running time of tools because an operating system scheduler's
granularity may be much larger than the time it takes to perform a
single IPMI message transaction. However, by spinning, your system
may be performing less useful work by not contexting out the tool for
a more useful task.
.LP
\fIauthcap\fR - This workaround flag will skip early checks for username
capabilities, authentication capabilities, and K_g support and allow
IPMI authentication to succeed. It works around multiple issues in
which the remote system does not properly report username
capabilities, authentication capabilities, or K_g status. Those
hitting this issue may see "username invalid", "authentication type
unavailable for attempted privilege level", or "k_g invalid" errors.
Issue observed on Asus P5M2/P5MT-R/RS162-E4/RX4, Intel SR1520ML/X38ML,
and Sun Fire 2200/4150/4450 with ELOM.
.LP
\fIidzero\fR - This workaround flag will allow empty session IDs to be
accepted by the client. It works around IPMI sessions that report
empty session IDs to the client. Those hitting this issue may see
"session timeout" errors. Issue observed on Tyan S2882 with M3289
BMC.
.LP
\fIunexpectedauth\fR - This workaround flag will allow unexpected non-null
authcodes to be checked as though they were expected. It works around
an issue when packets contain non-null authentication data when they
should be null due to disabled per-message authentication. Those
hitting this issue may see "session timeout" errors. Issue observed
on Dell PowerEdge 2850,SC1425. Confirmed fixed on newer firmware.
.LP
\fIforcepermsg\fR - This workaround flag will force per-message
authentication to be used no matter what is advertised by the remote
system. It works around an issue when per-message authentication is
advertised as disabled on the remote system, but it is actually
required for the protocol. Those hitting this issue may see "session
timeout" errors.  Issue observed on IBM eServer 325.
.LP
\fIendianseq\fR - This workaround flag will flip the endian of the session
sequence numbers to allow the session to continue properly. It works
around IPMI 1.5 session sequence numbers that are the wrong endian.
Those hitting this issue may see "session timeout" errors. Issue
observed on some Sun ILOM 1.0/2.0 (depends on service processor
endian).
.LP
\fIintel20\fR - This workaround flag will work around several Intel IPMI
2.0 authentication issues. The issues covered include padding of
usernames, and password truncation if the authentication algorithm is
HMAC-MD5-128. Those hitting this issue may see "username invalid",
"password invalid", or "k_g invalid" errors. Issue observed on Intel
SE7520AF2 with Intel Server Management Module (Professional Edition).
.LP
\fIsupermicro20\fR - This workaround flag will work around several
Supermicro IPMI 2.0 authentication issues on motherboards w/ Peppercon
IPMI firmware. The issues covered include handling invalid length
authentication codes. Those hitting this issue may see "password
invalid" errors.  Issue observed on Supermicro H8QME with SIMSO
daughter card. Confirmed fixed on newerver firmware.
.LP
\fIsun20\fR - This workaround flag will work work around several Sun IPMI
2.0 authentication issues. The issues covered include invalid
lengthed hash keys, improperly hashed keys, and invalid cipher suite
records. Those hitting this issue may see "password invalid" or "bmc
error" errors.  Issue observed on Sun Fire 4100/4200/4500 with ILOM.
This workaround automatically includes the "opensesspriv" workaround.
.LP
\fIopensesspriv\fR - This workaround flag will slightly alter
FreeIPMI's IPMI 2.0 connection protocol to workaround an invalid
hashing algorithm used by the remote system. The privilege level sent
during the Open Session stage of an IPMI 2.0 connection is used for
hashing keys instead of the privilege level sent during the RAKP1
connection stage. Those hitting this issue may see "password
invalid", "k_g invalid", or "bad rmcpplus status code" errors.  Issue
observed on Sun Fire 4100/4200/4500 with ILOM, Inventec 5441/Dell
Xanadu II, Supermicro X8DTH, Supermicro X8DTG, Intel S5500WBV/Penguin
Relion 700, Intel S2600JF/Appro 512X, and Quanta QSSC-S4R//Appro
GB812X-CN. This workaround is automatically triggered with the
"sun20" workaround.
.LP
\fIintegritycheckvalue\fR - This workaround flag will work around an
invalid integrity check value during an IPMI 2.0 session establishment
when using Cipher Suite ID 0. The integrity check value should be 0
length, however the remote motherboard responds with a non-empty
field. Those hitting this issue may see "k_g invalid" errors. Issue
observed on Supermicro X8DTG, Supermicro X8DTU, and Intel
S5500WBV/Penguin Relion 700, and Intel S2600JF/Appro 512X.
.LP
\fImalformedack\fR - This workaround flag will ignore malformed PET
acknowledge responses and assume any PET acknowledge response from the
remote machine is valid. It works around remote systems that respond
with PET acknowledge requests with invalid/malformed IPMI payloads.
Those hitting this issue may see "session timeout" errors when
executing a PET acknowledge. Issue observed on Dell Poweredge R610.
.LP
No IPMI 1.5 Support - Some motherboards that support IPMI 2.0 have
been found to not support IPMI 1.5. Those hitting this issue may see
"ipmi 2.0 unavailable" or "connection timeout" errors. This issue can
be worked around by using IPMI 2.0 instead of IPMI 1.5 by specifying
\fB\-\-driver\-address\fR=\fILAN_2_0\fR. Issue observed on HP
Proliant DL 145.
.SH "OEM INTERPRETATION"
The following motherboards are confirmed to have atleast some support
by the \fB\-\-interpret-oem-data\fR option. While highly probable the
OEM data interpretations would work across other motherboards by the
same manufacturer, there are no guarantees. Some of the motherboards
below may be rebranded by vendors/distributors.
.LP
Currently None
.SH "EXAMPLES"
.PP
Interpret a PET using the local SDR cache.
.PP
.B # ipmi-pet 356224 0x44 0x45 0x4c 0x4c 0x50 0x00 0x10 0x59 0x80 0x43 0xb2 0xc0 0x4f 0x33 0x33 0x58 0x00 0x02 0x19 0xe8 0x7e 0x26 0xff 0xff 0x20 0x20 0x04 0x20 0x73 0x18 0x00 0x80 0x01 0xff 0x00 0x00 0x00 0x00 0x00 0x19 0x00 0x00 0x02 0xa2 0x01 0x00 0xc1
.PP
Interpret a PET using a remote SDR cache.
.PP
.B # ipmi-pet -h ahost -u myusername -p mypassword 356224 0x44 0x45 0x4c 0x4c 0x50 0x00 0x10 0x59 0x80 0x43 0xb2 0xc0 0x4f 0x33 0x33 0x58 0x00 0x02 0x19 0xe8 0x7e 0x26 0xff 0xff 0x20 0x20 0x04 0x20 0x73 0x18 0x00 0x80 0x01 0xff 0x00 0x00 0x00 0x00 0x00 0x19 0x00 0x00 0x02 0xa2 0x01 0x00 0xc1
.PP
Interpret a PET using a previously stored SDR cache.
.PP
.B # ipmi-pet 356224 0x44 0x45 0x4c 0x4c 0x50 0x00 0x10 0x59 0x80 0x43 0xb2 0xc0 0x4f 0x33 0x33 0x58 0x00 0x02 0x19 0xe8 0x7e 0x26 0xff 0xff 0x20 0x20 0x04 0x20 0x73 0x18 0x00 0x80 0x01 0xff 0x00 0x00 0x00 0x00 0x00 0x19 0x00 0x00 0x02 0xa2 0x01 0x00 0xc1 --sdr-cache-file=/tmp/mysdrcache
.PP
Instead of outputting trap interpretation, send a PET acknowledge using the trap data.
.PP
.B # ipmi-pet -h ahost --pet-acknowledge 356224 0x44 0x45 0x4c 0x4c 0x50 0x00 0x10 0x59 0x80 0x43 0xb2 0xc0 0x4f 0x33 0x33 0x58 0x00 0x02 0x19 0xe8 0x7e 0x26 0xff 0xff 0x20 0x20 0x04 0x20 0x73 0x18 0x00 0x80 0x01 0xff 0x00 0x00 0x00 0x00 0x00 0x19 0x00 0x00 0x02 0xa2 0x01 0x00 0xc1
.SH "REPORTING BUGS"
Report bugs to <freeipmi\-users@gnu.org> or <freeipmi\-devel@gnu.org>.
.SH "COPYRIGHT"
Copyright \(co 2011-2012 FreeIPMI Core Team
.PP
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.
.SH "SEE ALSO"
freeipmi(7), bmc-info(8), ipmi-pef-config(8), ipmi-sel(8),
freeipmi_interpret_sel.conf(5)
.PP
http://www.gnu.org/software/freeipmi/
