.TP
\fB\-\-disable\-auto\-probe\fR
Do not probe in-band IPMI devices for default settings.
.TP
\fB\-\-driver\-address\fR=\fIDRIVER-ADDRESS\fR
Specify the in-band driver address to be used instead of the probed
value.  \fIDRIVER-ADDRESS\fR should be prefixed with "0x" for a hex 
value and '0' for an octal value.
.TP
\fB\-\-driver\-device\fR=\fIDEVICE\fR 
Specify the in-band driver device path to be used instead of the
probed path.
.TP
\fB\-\-register\-spacing\fR=\fIREGISTER-SPACING\fR
Specify the in-band driver register spacing instead of the probed
value.  Argument is in bytes (i.e. 32bit register spacing = 4)
