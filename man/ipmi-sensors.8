.TH IPMI-SENSORS 8 "2012-06-28" "IPMI Sensors version 1.1.6" "System Commands"
.SH "NAME"
ipmi-sensors \- display IPMI sensor information
.SH "SYNOPSIS"
.B ipmi-sensors
[\fIOPTION\fR...]
.SH "DESCRIPTION"
.B Ipmi-sensors
displays current readings of sensors and sensor data repository (SDR)
information. The default display outputs each sensor's record id,
sensor name, sensor type name, sensor reading (if appropriate), and
the current sensor event. More verbose information can be found using
the verbose options specified below.

.B Ipmi-sensors
does not inform the user if a problem exists with a particular sensor
because sensor readings and events are not analyzed by default. Users
may wish to use the \fB\-\-output\-sensor\-state\fR option to output
the analyzed sensor state.

Some sensors may have a sensor reading or sensor event of "N/A" if
the information is unavailable. This is typical of a sensor that
is not enabled or not owned by a BMC. Please see
\fB\-\-bridge\-sensors\fR option below to deal with sensors not owned
by a BMC. Sensors may output a sensor event of "Unknown" if the
sensor reading cannot be read. This is typical of a sensor that is
busy or a reading that cannot be calculated. If sensors report
"Unrecognized State", it is indicative of an unkown sensor type,
typically an OEM sensor. If the sensor OEM interpretation is
available, the \fB\-\-intepret\-oem\-data\fR may be able to report
the appropriate sensor state. Sensors need not always report a sensor
event. When a sensor event is not present, "OK" is typically
reported.
.LP
Listed below are general IPMI options, tool specific options, trouble
shooting information, workaround information, examples, and known
issues. For a general introduction to FreeIPMI please see
.B freeipmi(7).
To perform IPMI sensor configuration, please see
.B ipmi-sensors-config(8).
To perform some advanced SDR management, please see
.B bmc-device(8).
.SH "GENERAL OPTIONS"
The following options are general options for configuring IPMI
communication and executing general tool commands.
.TP
\fB\-D\fR \fIIPMIDRIVER\fR, \fB\-\-driver\-type\fR=\fIIPMIDRIVER\fR
Specify the driver type to use instead of doing an auto selection.
The currently available outofband drivers are LAN and LAN_2_0, which
perform IPMI 1.5 and IPMI 2.0 respectively. The currently available
inband drivers are KCS, SSIF, OPENIPMI, and SUNBMC.
.TP
\fB\-\-disable\-auto\-probe\fR
Do not probe in-band IPMI devices for default settings.
.TP
\fB\-\-driver\-address\fR=\fIDRIVER-ADDRESS\fR
Specify the in-band driver address to be used instead of the probed
value. \fIDRIVER-ADDRESS\fR should be prefixed with "0x" for a hex
value and '0' for an octal value.
.TP
\fB\-\-driver\-device\fR=\fIDEVICE\fR
Specify the in-band driver device path to be used instead of the
probed path.
.TP
\fB\-\-register\-spacing\fR=\fIREGISTER-SPACING\fR
Specify the in-band driver register spacing instead of the probed
value. Argument is in bytes (i.e. 32bit register spacing = 4)
.TP
\fB\-h\fR \fIIPMIHOST1,IPMIHOST2,...\fR, \fB\-\-hostname\fR=\fIIPMIHOST1,IPMIHOST2,...\fR
Specify the remote host(s) to communicate with. Multiple hostnames
may be separated by comma or may be specified in a range format; see
HOSTRANGED SUPPORT below.
.TP
\fB\-u\fR \fIUSERNAME\fR, \fB\-\-username\fR=\fIUSERNAME\fR
Specify the username to use when authenticating with the remote host.
If not specified, a null (i.e. anonymous) username is assumed. The
user must have atleast OPERATOR privileges in order for this tool to
operate fully.
.TP
\fB\-p\fR \fIPASSWORD\fR, \fB\-\-password\fR=\fIPASSWORD\fR
Specify the password to use when authenticationg with the remote host.
If not specified, a null password is assumed. Maximum password length
is 16 for IPMI 1.5 and 20 for IPMI 2.0.
.TP
\fB\-P\fR, \fB\-\-password-prompt\fR
Prompt for password to avoid possibility of listing
it in process lists.
.TP
\fB\-k\fR \fIK_G\fR, \fB\-\-k-g\fR=\fIK_G\fR
Specify the K_g BMC key to use when authenticating with the remote
host for IPMI 2.0. If not specified, a null key is assumed. To input
the key in hexadecimal form, prefix the string with '0x'. E.g., the
key 'abc' can be entered with the either the string 'abc' or the
string '0x616263'
.TP
\fB\-K\fR, \fB\-\-k-g-prompt\fR
Prompt for k-g to avoid possibility of listing it in process lists.
.TP
\fB\-\-session-timeout\fR=\fIMILLISECONDS\fR
Specify the session timeout in milliseconds. Defaults to 20000
milliseconds (20 seconds) if not specified.
.TP
\fB\-\-retransmission-timeout\fR=\fIMILLISECONDS\fR
Specify the packet retransmission timeout in milliseconds. Defaults
to 1000 milliseconds (1 second) if not specified. The retransmission
timeout cannot be larger than the session timeout.
.TP
\fB\-a\fR \fIAUTHENTICATION\-TYPE\fR, \fB\-\-authentication\-type\fR=\fIAUTHENTICATION\-TYPE\fR
Specify the IPMI 1.5 authentication type to use. The currently
available authentication types are NONE, STRAIGHT_PASSWORD_KEY, MD2,
and MD5. Defaults to MD5 if not specified.
.TP
\fB\-I\fR \fICIPHER-SUITE-ID\fR, \fB\-\-cipher\-suite-id\fR=\fICIPHER-SUITE-ID\fR
Specify the IPMI 2.0 cipher suite ID to use. The Cipher Suite ID
identifies a set of authentication, integrity, and confidentiality
algorithms to use for IPMI 2.0 communication. The authentication
algorithm identifies the algorithm to use for session setup, the
integrity algorithm identifies the algorithm to use for session packet
signatures, and the confidentiality algorithm identifies the algorithm
to use for payload encryption. Defaults to cipher suite ID 3 if not
specified. The following cipher suite ids are currently supported:
.sp
0 - Authentication Algorithm = None; Integrity Algorithm = None; Confidentiality Algorithm = None
.sp
1 - Authentication Algorithm = HMAC-SHA1; Integrity Algorithm = None; Confidentiality Algorithm = None
.sp
2 - Authentication Algorithm = HMAC-SHA1; Integrity Algorithm = HMAC-SHA1-96; Confidentiality Algorithm = None
.sp
3 - Authentication Algorithm = HMAC-SHA1; Integrity Algorithm = HMAC-SHA1-96; Confidentiality Algorithm = AES-CBC-128
.\" .sp
.\" 4 - Authentication Algorithm = HMAC-SHA1; Integrity Algorithm = HMAC-SHA1-96; Confidentiality Algorithm = xRC4-128
.\" .sp
.\" 5 - Authentication Algorithm = HMAC-SHA1; Integrity Algorithm = HMAC-SHA1-96; Confidentiality Algorithm = xRC4-40
.sp
6 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = None; Confidentiality Algorithm = None
.sp
7 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = HMAC-MD5-128; Confidentiality Algorithm = None
.sp
8 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = HMAC-MD5-128; Confidentiality Algorithm = AES-CBC-128
.\" .sp
.\" 9 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = HMAC-MD5-128; Confidentiality Algorithm = xRC4-128
.\" .sp
.\" 10 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = HMAC-MD5-128; Confidentiality Algorithm = xRC4-40
.sp
11 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = MD5-128; Confidentiality Algorithm = None
.sp
12 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = MD5-128; Confidentiality Algorithm = AES-CBC-128
.\" .sp
.\" 13 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = MD5-128; Confidentiality Algorithm = xRC4-128
.\" .sp
.\" 14 - Authentication Algorithm = HMAC-MD5; Integrity Algorithm = MD5-128; Confidentiality Algorithm = xRC4-40
.\" XXX GUESS
.\" .sp
.\" 15 - Authentication Algorithm = HMAC-SHA256; Integrity Algorithm = None; Confidentiality Algorithm = None
.\" XXX GUESS
.\" .sp
.\" 16 - Authentication Algorithm = HMAC-SHA256; Integrity Algorithm = HMAC_SHA256_128; Confidentiality Algorithm = None
.sp
17 - Authentication Algorithm = HMAC-SHA256; Integrity Algorithm = HMAC_SHA256_128; Confidentiality Algorithm = AES-CBC-128
.\" XXX GUESS
.\" .sp
.\" 18 - Authentication Algorithm = HMAC-SHA256; Integrity Algorithm = HMAC_SHA256_128; Confidentiality Algorithm = xRC4-128
.\" XXX GUESS
.\" .sp
.\" 19 - Authentication Algorithm = HMAC-SHA256; Integrity Algorithm = HMAC_SHA256_128; Confidentiality Algorithm = xRC4-40
.TP
\fB\-l\fR \fIPRIVILEGE\-LEVEL\fR, \fB\-\-privilege\-level\fR=\fIPRIVILEGE\-LEVEL\fR
Specify the privilege level to be used. The currently available
privilege levels are USER, OPERATOR, and ADMIN. Defaults to OPERATOR
if not specified.
.TP
\fB\-\-config\-file\fR=\fIFILE\fR
Specify an alternate configuration file.
.TP
\fB\-W\fR \fIWORKAROUNDS\fR, \fB\-\-workaround\-flags\fR=\fIWORKAROUNDS\fR
Specify workarounds to vendor compliance issues. Multiple workarounds
can be specified separated by commas. A special command line flag of
"none", will indicate no workarounds (may be useful for overriding
configured defaults). See WORKAROUNDS below for a list of available
workarounds.
.TP
\fB\-\-debug\fR
Turn on debugging.
.TP
\fB\-?\fR, \fB\-\-help\fR
Output a help list and exit.
.TP
\fB\-\-usage\fR
Output a usage message and exit.
.TP
\fB\-V\fR, \fB\-\-version\fR
Output the program version and exit.
.SH "IPMI-SENSORS OPTIONS"
The following options are specific to
.B Ipmi-sensors.
.TP
\fB\-v\fR, \fB\-\-verbose\fR
Output verbose sensor output. This option will output additional
information about sensors such as thresholds, ranges, numbers, and
event/reading type codes.
.TP
\fB\-vv\fR
Output very verbose sensor output. This option will output more
additional information than the verbose option such as information
about events, other sensor types, and oem sensors.
.TP
\fB\-i\fR, \fB\-\-sdr\-info\fR
Show sensor data repository (SDR) information
.TP
\fB\-q\fR, \fB\-\-quiet-readings\fR
Do not output sensor reading values by default. This option is
particularly useful if you want to use hostranged output across a
cluster and want to consolidate the output.
.TP
\fB\-r\fR \fIRECORD\-IDS\-LIST\fR, \fB\-\-record\-ids\fR=\fIRECORD\-IDS\-LIST\fR
Specify sensors to show by record id. Multiple record ids can be
separated by commas or spaces. If both \fB\-\-record\-ids\fR and
\fB\-\-sensor\-types\fR are specified, \fB\-\-record\-ids\fR takes
precedence. A special command line record id of "all", will indicate
all record ids should be shown (may be useful for overriding
configured defaults).
.TP
\fB\-R\fR \fIRECORD\-IDS\-LIST\fR, \fB\-\-exclude\-record\-ids\fR=\fIRECORD\-IDS\-LIST\fR
Specify sensors to not show by record id. Multiple record ids can be
separated by commas or spaces. A special command line record id of
"none", will indicate no record ids should be excluded (may be useful
for overriding configured defaults).
.TP
\fB\-t\fR \fISENSOR\-TYPE\-LIST\fR, \fB\-\-sensor\-types\fR=\fISENSOR\-TYPE\-LIST\fR
Specify sensor types to show outputs for. Multiple types can
be separated by commas or spaces. If both \fB\-\-record\-ids\fR and
\fB\-\-sensor\-types\fR are specified, \fB\-\-record\-ids\fR takes precedence.
A special command line type of "all", will indicate all types should
be shown (may be useful for overriding configured defaults). Users
may specify sensor types by string (see \fB\-\-list\-sensor\-types\fR
below) or by number (decimal or hex).
.TP
\fB\-T\fR \fISENSOR\-TYPE\-LIST\fR, \fB\-\-exclude\-sensor\-types\fR=\fISENSOR\-TYPE\-LIST\fR
Specify sensor types to not show outputs for. Multiple types
can be eparated by commas or spaces. A special command line type of
"none", will indicate no types should be excluded (may be useful for
overriding configured defaults). Users may specify sensor types by
string (see \fB\-\-list\-sensor\-types\fR below) or by number (decimal
or hex).
.TP
\fB\-L\fR, \fB\-\-list\-sensor\-types\fR
List sensor types.
.TP
\fB\-b\fR, \fB\-\-bridge\-sensors\fR
By default, sensors readings are not attempted for sensors on non-BMC
owners. By setting this option, sensor requests can be bridged to
non-BMC owners to obtain sensor readings. Bridging may not work on
some interfaces/driver types.
.TP
\fB\-\-shared\-sensors\fR
Some sensors share the same sensor data record (SDR). This is
typically utilized for system event log (SEL) entries and not for
sensor readings. However, there may be some motherboards in which
this format is utilized for multiple active sensors, or the user
simply has interest in seeing the permutation of entries shared by a
SDR entry. By setting this option, each sensor number shared by a
record will be iterated over and output.
.TP
\fB\-\-interpret\-oem\-data\fR
Attempt to interpret OEM data, such as event data, sensor readings, or
general extra info, etc. If an OEM interpretation is not available,
the default output will be generated. Correctness of OEM
interpretations cannot be guaranteed due to potential changes OEM
vendors may make in products, firmware, etc. See OEM INTERPRETATION
below for confirmed supported motherboard interpretations.
.TP
\fB\-\-ignore\-not\-available\-sensors\fR
Ignore not-available (i.e. N/A) sensors in output.
.TP
\fB\-\-ignore\-unrecognized\-events\fR
Ignore unrecognized sensor events. This will suppress output of
unrecognized events, typically shown as 'Unrecognized Event = XXXXh'
in output. In addition, unrecognized events will be ignored when
calculating sensor state with \fI\-\-output\-sensor\-state\fR below.
.TP
\fB\-\-output\-event\-bitmask\fR
Output event bitmask value instead of the string representation.
.TP
\fB\-\-output\-sensor\-state\fR
Output sensor state in output. This will add an additional output
reporting if a sensor is in a NOMINAL, WARNING, or CRITICAL state.
The sensor state is an interpreted value based on the current sensor
event. The sensor state interpretations are determined by the
configuration file /usr/local/etc/freeipmi//freeipmi_interpret_sensor.conf.
See
.B freeipmi_interpret_sensor.conf(5)
for more information.
This option gives identical output to the sensor state
previously output by
.B ipmimonitoring(8).
.TP
\fB\-\-sensor\-state\-config\-file\fR=\fIFILE\fR
Specify an alternate sensor state configuration file. Option ignored
if \fB\-\-output\-sensor\-state\fR not specified.
.TP
\fB\-\-entity\-sensor\-names\fR
Output sensor names prefixed with their entity id and instance number
when appropriate. This may be necessary on some motherboards to help
identify what sensors are referencing. For example, a motherboard may
have multiple sensors named 'TEMP'. The entity id and instance number
may help clarify which sensor refers to "Processor 1" vs. "Processor
2".
.TP
\fB\-\-no\-sensor\-type\-output\fR
Do not show sensor type output for each entry. On many systems, the
sensor type is redundant to the name of the sensor. This can
especially be true if \fB\-\-entity\-sensor\-names\fR is specified.
If the sensor name is sufficient, or if the sensor type is of no
interest to the user, this option can be specified to condense output.
.TP
\fB\-\-comma\-separated\-output
Output fields in comma separated format.
.TP
\fB\-\-no\-header\-output
Do not output column headers. May be useful in scripting.
.TP
\fB\-\-non\-abbreviated\-units\fR
Output non-abbreviated units (e.g. 'Amps' instead of 'A'). May aid in
disambiguation of units (e.g. 'C' for Celsius or Coulombs).
.TP
\fB\-\-legacy\-output\fR
Output in legacy format. Newer options may not be applicable to
legacy output.
.TP
\fB\-\-ipmimonitoring\-legacy\-output\fR
Output legacy format of legacy
.B ipmimonitoring
tool. Newer options may not be applicable to legacy output.
.SH "SDR CACHE OPTIONS"
This tool requires access to the sensor data repository (SDR) cache
for general operation. By default, SDR data will be downloaded and
cached on the local machine. The following options apply to the SDR
cache.
.TP
\fB\-f\fR, \fB\-\-flush\-cache\fR
Flush a cached version of the sensor data repository (SDR) cache. The
SDR is typically cached for faster subsequent access. However, it may
need to be flushed and re-generated if the SDR has been updated on a
system.
.TP
\fB\-Q\fR, \fB\-\-\quiet\-cache\fR
Do not output information about cache creation/deletion. May be
useful in scripting.
.TP
\fB\-\-sdr\-cache\-directory\fR=\fIDIRECTORY\fR
Specify an alternate directory for sensor data repository (SDR) caches
to be stored or read from. Defaults to the home directory if not
specified.
.TP
\fB\-\-sdr\-cache\-file\fR=\fIFILE\fR
Specify a specific sensor data repository (SDR) cache file to be
stored or read from.
.TP
\fB\-\-sdr-cache-recreate\fR
If the SDR cache is out of date or invalid, automatically recreate the
sensor data repository (SDR) cache. This option may be useful for
scripting purposes.
.SH "HOSTRANGED OPTIONS"
The following options manipulate hostranged output. See HOSTRANGED
SUPPORT below for additional information on hostranges.
.TP
\fB\-B\fR, \fB\-\-buffer-output\fR
Buffer hostranged output. For each node, buffer standard output until
the node has completed its IPMI operation. When specifying this
option, data may appear to output slower to the user since the the
entire IPMI operation must complete before any data can be output.
See HOSTRANGED SUPPORT below for additional information.
.TP
\fB\-C\fR, \fB\-\-consolidate-output\fR
Consolidate hostranged output. The complete standard output from
every node specified will be consolidated so that nodes with identical
output are not output twice. A header will list those nodes with the
consolidated output. When this option is specified, no output can be
seen until the IPMI operations to all nodes has completed. If the
user breaks out of the program early, all currently consolidated
output will be dumped. See HOSTRANGED SUPPORT below for additional
information.
.TP
\fB\-F\fR \fINUM\fR, \fB\-\-fanout\fR=\fINUM\fR
Specify multiple host fanout. A "sliding window" (or fanout)
algorithm is used for parallel IPMI communication so that slower nodes
or timed out nodes will not impede parallel communication. The
maximum number of threads available at the same time is limited by the
fanout. The default is 64.
.TP
\fB\-E\fR, \fB\-\-eliminate\fR
Eliminate hosts determined as undetected by
.B ipmidetect.
This attempts to remove the common issue of hostranged execution
timing out due to several nodes being removed from service in a large
cluster. The
.B ipmidetectd
daemon must be running on the node executing the command.
.TP
\fB\-\-always\-prefix\fR
Always prefix output, even if only one host is specified or
communicating in-band. This option is primarily useful for
scripting purposes. Option will be ignored if specified with
the \fB\-C\fR option.
.SH "HOSTRANGED SUPPORT"
Multiple hosts can be input either as an explicit comma separated
lists of hosts or a range of hostnames in the general form:
prefix[n-m,l-k,...], where n < m and l < k, etc. The later form
should not be confused with regular expression character classes (also
denoted by []). For example, foo[19] does not represent foo1 or foo9,
but rather represents a degenerate range: foo19.
.LP
This range syntax is meant only as a convenience on clusters with a
prefixNN naming convention and specification of ranges should not be
considered necessary -- the list foo1,foo9 could be specified as such,
or by the range foo[1,9].
.LP
Some examples of range usage follow:
.nf
    foo[01-05] instead of foo01,foo02,foo03,foo04,foo05
    foo[7,9-10] instead of foo7,foo9,foo10
    foo[0-3] instead of foo0,foo1,foo2,foo3
.fi
.LP
As a reminder to the reader, some shells will interpret brackets ([
and ]) for pattern matching. Depending on your shell, it may be
necessary to enclose ranged lists within quotes.
.LP
When multiple hosts are specified by the user, a thread will be
executed for each host in parallel up to the configured fanout (which
can be adjusted via the \fB\-F\fR option). This will allow
communication to large numbers of nodes far more quickly than if done
in serial.
.LP
By default, standard output from each node specified will be output
with the hostname prepended to each line. Although this output is
readable in many situations, it may be difficult to read in other
situations. For example, output from multiple nodes may be mixed
together. The \fB\-B\fR and \fB\-C\fR options can be used to change
this default.
.LP
In-band IPMI Communication will be used when the host "localhost" is
specified. This allows the user to add the localhost into the
hostranged output.
.SH "GENERAL TROUBLESHOOTING"
Most often, IPMI problems are due to configuration problems.
.LP
IPMI over LAN problems involve a misconfiguration of the remote
machine's BMC.  Double check to make sure the following are configured
properly in the remote machine's BMC: IP address, MAC address, subnet
mask, username, user enablement, user privilege, password, LAN
privilege, LAN enablement, and allowed authentication type(s). For
IPMI 2.0 connections, double check to make sure the cipher suite
privilege(s) and K_g key are configured properly. The
.B bmc-config(8)
tool can be used to check and/or change these configuration
settings.
.LP
Inband IPMI problems are typically caused by improperly configured
drivers or non-standard BMCs.
.LP
In addition to the troubleshooting tips below, please see WORKAROUNDS
below to also if there are any vendor specific bugs that have been
discovered and worked around.
.LP
Listed below are many of the common issues for error messages.
For additional support, please e-mail the <freeipmi\-users@gnu.org>
mailing list.
.LP
"username invalid" - The username entered (or a NULL username if none
was entered) is not available on the remote machine. It may also be
possible the remote BMC's username configuration is incorrect.
.LP
"password invalid" - The password entered (or a NULL password if none
was entered) is not correct. It may also be possible the password for
the user is not correctly configured on the remote BMC.
.LP
"password verification timeout" - Password verification has timed out.
A "password invalid" error (described above) or a generic "session
timeout" (described below) occurred.  During this point in the
protocol it cannot be differentiated which occurred.
.LP
"k_g invalid" - The K_g key entered (or a NULL K_g key if none was
entered) is not correct. It may also be possible the K_g key is not
correctly configured on the remote BMC.
.LP
"privilege level insufficient" - An IPMI command requires a higher
user privilege than the one authenticated with. Please try to
authenticate with a higher privilege. This may require authenticating
to a different user which has a higher maximum privilege.
.LP
"privilege level cannot be obtained for this user" - The privilege
level you are attempting to authenticate with is higher than the
maximum allowed for this user. Please try again with a lower
privilege. It may also be possible the maximum privilege level
allowed for a user is not configured properly on the remote BMC.
.LP
"authentication type unavailable for attempted privilege level" - The
authentication type you wish to authenticate with is not available for
this privilege level. Please try again with an alternate
authentication type or alternate privilege level. It may also be
possible the available authentication types you can authenticate with
are not correctly configured on the remote BMC.
.LP
"cipher suite id unavailable" - The cipher suite id you wish to
authenticate with is not available on the remote BMC. Please try
again with an alternate cipher suite id. It may also be possible the
available cipher suite ids are not correctly configured on the remote
BMC.
.LP
"ipmi 2.0 unavailable" - IPMI 2.0 was not discovered on the remote
machine. Please try to use IPMI 1.5 instead.
.LP
"connection timeout" - Initial IPMI communication failed. A number of
potential errors are possible, including an invalid hostname
specified, an IPMI IP address cannot be resolved, IPMI is not enabled
on the remote server, the network connection is bad, etc. Please
verify configuration and connectivity.
.LP
"session timeout" - The IPMI session has timed out. Please reconnect.
If this error occurs often, you may wish to increase the
retransmission timeout. Some remote BMCs are considerably slower than
others.
.LP
"device not found" - The specified device could not be found. Please
check configuration or inputs and try again.
.LP
"driver timeout" - Communication with the driver or device has timed
out. Please try again.
.LP
"message timeout" - Communication with the driver or device has timed
out. Please try again.
.LP
"BMC busy" - The BMC is currently busy. It may be processing
information or have too many simultaneous sessions to manage. Please
wait and try again.
.LP
"could not find inband device" - An inband device could not be found.
Please check configuration or specify specific device or driver on the
command line.
.LP
"driver timeout" - The inband driver has timed out communicating to
the local BMC or service processor. The BMC or service processor may
be busy or (worst case) possibly non-functioning.
.LP
"sensor config file parse error" - A parse error was found in the
sensor interpretation configuration file. Please see
.B freeipmi_interpret_sensor.conf(5).
.SH "WORKAROUNDS"
With so many different vendors implementing their own IPMI solutions,
different vendors may implement their IPMI protocols incorrectly. The
following describes a number of workarounds currently available to
handle discovered compliance issues. When possible, workarounds have
been implemented so they will be transparent to the user. However,
some will require the user to specify a workaround be used via the -W
option.
.LP
The hardware listed below may only indicate the hardware that a
problem was discovered on. Newer versions of hardware may fix the
problems indicated below. Similar machines from vendors may or may
not exhibit the same problems. Different vendors may license their
firmware from the same IPMI firmware developer, so it may be
worthwhile to try workarounds listed below even if your motherboard is
not listed.
.LP
If you believe your hardware has an additional compliance issue that
needs a workaround to be implemented, please contact the FreeIPMI
maintainers on <freeipmi\-users@gnu.org> or <freeipmi\-devel@gnu.org>.
.LP
\fIassumeio\fR - This workaround flag will assume inband interfaces
communicate with system I/O rather than being memory-mapped. This
will work around systems that report invalid base addresses. Those
hitting this issue may see "device not supported" or "could not find
inband device" errors.  Issue observed on HP ProLiant DL145 G1.
.LP
\fIspinpoll\fR - This workaround flag will inform some inband drivers
(most notably the KCS driver) to spin while polling rather than
putting the process to sleep. This may significantly improve the wall
clock running time of tools because an operating system scheduler's
granularity may be much larger than the time it takes to perform a
single IPMI message transaction. However, by spinning, your system
may be performing less useful work by not contexting out the tool for
a more useful task.
.LP
\fIauthcap\fR - This workaround flag will skip early checks for username
capabilities, authentication capabilities, and K_g support and allow
IPMI authentication to succeed. It works around multiple issues in
which the remote system does not properly report username
capabilities, authentication capabilities, or K_g status. Those
hitting this issue may see "username invalid", "authentication type
unavailable for attempted privilege level", or "k_g invalid" errors.
Issue observed on Asus P5M2/P5MT-R/RS162-E4/RX4, Intel SR1520ML/X38ML,
and Sun Fire 2200/4150/4450 with ELOM.
.LP
\fIidzero\fR - This workaround flag will allow empty session IDs to be
accepted by the client. It works around IPMI sessions that report
empty session IDs to the client. Those hitting this issue may see
"session timeout" errors. Issue observed on Tyan S2882 with M3289
BMC.
.LP
\fIunexpectedauth\fR - This workaround flag will allow unexpected non-null
authcodes to be checked as though they were expected. It works around
an issue when packets contain non-null authentication data when they
should be null due to disabled per-message authentication. Those
hitting this issue may see "session timeout" errors. Issue observed
on Dell PowerEdge 2850,SC1425. Confirmed fixed on newer firmware.
.LP
\fIforcepermsg\fR - This workaround flag will force per-message
authentication to be used no matter what is advertised by the remote
system. It works around an issue when per-message authentication is
advertised as disabled on the remote system, but it is actually
required for the protocol. Those hitting this issue may see "session
timeout" errors.  Issue observed on IBM eServer 325.
.LP
\fIendianseq\fR - This workaround flag will flip the endian of the session
sequence numbers to allow the session to continue properly. It works
around IPMI 1.5 session sequence numbers that are the wrong endian.
Those hitting this issue may see "session timeout" errors. Issue
observed on some Sun ILOM 1.0/2.0 (depends on service processor
endian).
.LP
\fIintel20\fR - This workaround flag will work around several Intel IPMI
2.0 authentication issues. The issues covered include padding of
usernames, and password truncation if the authentication algorithm is
HMAC-MD5-128. Those hitting this issue may see "username invalid",
"password invalid", or "k_g invalid" errors. Issue observed on Intel
SE7520AF2 with Intel Server Management Module (Professional Edition).
.LP
\fIsupermicro20\fR - This workaround flag will work around several
Supermicro IPMI 2.0 authentication issues on motherboards w/ Peppercon
IPMI firmware. The issues covered include handling invalid length
authentication codes. Those hitting this issue may see "password
invalid" errors.  Issue observed on Supermicro H8QME with SIMSO
daughter card. Confirmed fixed on newerver firmware.
.LP
\fIsun20\fR - This workaround flag will work work around several Sun IPMI
2.0 authentication issues. The issues covered include invalid
lengthed hash keys, improperly hashed keys, and invalid cipher suite
records. Those hitting this issue may see "password invalid" or "bmc
error" errors.  Issue observed on Sun Fire 4100/4200/4500 with ILOM.
This workaround automatically includes the "opensesspriv" workaround.
.LP
\fIopensesspriv\fR - This workaround flag will slightly alter
FreeIPMI's IPMI 2.0 connection protocol to workaround an invalid
hashing algorithm used by the remote system. The privilege level sent
during the Open Session stage of an IPMI 2.0 connection is used for
hashing keys instead of the privilege level sent during the RAKP1
connection stage. Those hitting this issue may see "password
invalid", "k_g invalid", or "bad rmcpplus status code" errors.  Issue
observed on Sun Fire 4100/4200/4500 with ILOM, Inventec 5441/Dell
Xanadu II, Supermicro X8DTH, Supermicro X8DTG, Intel S5500WBV/Penguin
Relion 700, Intel S2600JF/Appro 512X, and Quanta QSSC-S4R//Appro
GB812X-CN. This workaround is automatically triggered with the
"sun20" workaround.
.LP
\fIintegritycheckvalue\fR - This workaround flag will work around an
invalid integrity check value during an IPMI 2.0 session establishment
when using Cipher Suite ID 0. The integrity check value should be 0
length, however the remote motherboard responds with a non-empty
field. Those hitting this issue may see "k_g invalid" errors. Issue
observed on Supermicro X8DTG, Supermicro X8DTU, and Intel
S5500WBV/Penguin Relion 700, and Intel S2600JF/Appro 512X.
.LP
\fIdiscretereading\fR - This workaround option will allow analog
sensor readings (i.e. rpm, degrees, etc.) to be read even if the
event/reading type code for the sensor is for a discrete sensor
(i.e. assert vs. deassert). This option works around poorly defined
(and arguably illegal) SDR records that expect analog sensor readings
to be read alongside discrete sensors. This option is confirmed to
work around issues on HP Proliant DL380 G7 and HP ProLiant ML310 G5
motherboards.
.LP
\fIignorescanningdisabled\fR - This workaround option will allow sensor
readings to be read even if the sensor scanning bit indicates a sensor
is disabled. This option works around motherboards that incorrectly
indicate sensors as disabled. This may problem may exist on your
motherboard if sensors are listed as "N/A" even if they should be
available. This option is confirmed to work around issues on Dell
Poweredge 2900, Dell Poweredge 2950, Dell Poweredge R410, Dell
Poweredge R610, and HP Integrity rx3600 motherboards.
.LP
\fIassumebmcowner\fR - This workaround option will allow sensor
readings to be read if the sensor owner is the BMC, but the reported
sensor owner is not the BMC. Typically, sensors owned by a non-BMC
sensor owner must be bridged (e.g. with the \fB\-\-bridge\-sensors\fR
option), however if the non-BMC sensor owner is invalid, bridging
fails. This option works around motherboards that incorrectly report
an non-BMC sensor owner by always assuming the sensor owner is the
BMC. This problem may exist on your motherboard if sensors are listed
as "N/A" even if they should be available. This option is confirmed to
work around issues on Fujitsu RX300 and Fujitsu RX300S2 motherboards.
.LP
\fIignoreauthcode\fR - This workaround option will allow sensor
readings to be read if the remote machine is invalidly calculating
authentication codes (i.e. authentication hashes) when communicating
over LAN. This problem may exist on your system if the error "session
timeout" errors or there is an appearance of a hang.  Users are
cautioned on the use of this option, as it removes an authentication
check verifying the validity of a packet. However, in most
organizations, this is unlikely to be a security issue. The ignoring
of authentication packets is only limited to the period in which
sensor readings are done, and not for any portion of the session
authentication or session teardown. This option is confirmed to work
on Inventec 5441/Dell Xanadu II and Inventec 5442/Dell Xanadu III.
(Note: On the above systems, this issue has only been observed when
the \fB\-\-bridge\-sensors\fR is used.)
.LP
No IPMI 1.5 Support - Some motherboards that support IPMI 2.0 have
been found to not support IPMI 1.5. Those hitting this issue may see
"ipmi 2.0 unavailable" or "connection timeout" errors. This issue can
be worked around by using IPMI 2.0 instead of IPMI 1.5 by specifying
\fB\-\-driver\-address\fR=\fILAN_2_0\fR. Issue observed on HP
Proliant DL 145.
.SH "OEM INTERPRETATION"
The following motherboards are confirmed to have atleast some support
by the \fB\-\-interpret-oem-data\fR option. While highly probable the
OEM data interpretations would work across other motherboards by the
same manufacturer, there are no guarantees. Some of the motherboards
below may be rebranded by vendors/distributors.
.LP
Dell Poweredge R210, Dell Poweredge R610, Dell Poweredge R710, Fujitsu
iRMC S1 and iRMC S2 systems, Intel S5500WB/Penguin Computing Relion
700, Intel S2600JF/Appro 512X, Intel S5000PAL, Supermicro X7DBR-3,
Supermicro X7DB8, Supermicro X8DTN, Supermicro X7SBI-LN4, Supermicro
X8DTH, Supermicro X8DTG, Supermicro X8DTU, Supermicro X8DT3-LN4F,
Supermicro X8DTU-6+, Supermicro X8DTL, Supermicro X8DTL-3F, Supermicro
X8SIL-F, Supermicro X9SCL, Supermicro X9SCM, Supermicro X8DTN+-F,
Supermicro X8SIE, Supermicro X9SCA-F-O, Supermicro H8DGU-F, Supermicro
X9DRi-F.
.SH "EXAMPLES"
.B # ipmi-sensors
.PP
Show all sensors and readings on the local machine.
.PP
.B # ipmi-sensors --verbose
.PP
Show verbose sensors and readings on the local machine.
.PP
.B # ipmi-sensors --record-ids="7,11,102"
.PP
Show sensor record ids 7, 11, and 102 on the local machine.
.PP
.B # ipmi-sensors --sensor-types=fan
.PP
Show all sensors of type fan on the local machine.
.PP
.B # ipmi-sensors -h ahost -u myusername -p mypassword
.PP
Show all sensors on a remote machine using IPMI over LAN.
.PP
.B # ipmi-sensors -h mycluster[0-127] -u myusername -p mypassword
.PP
Show all sensors across a cluster using IPMI over LAN.
.PP
.SH "KNOWN ISSUES"
On older operating systems, if you input your username, password,
and other potentially security relevant information on the command
line, this information may be discovered by other users when using
tools like the
.B ps(1)
command or looking in the /proc file system. It is generally more
secure to input password information with options like the -P or -K
options. Configuring security relevant information in the FreeIPMI
configuration file would also be an appropriate way to hide this information.
.LP
In order to prevent brute force attacks, some BMCs will temporarily
"lock up" after a number of remote authentication errors. You may
need to wait awhile in order to this temporary "lock up" to pass
before you may authenticate again.
.LP
Some sensors may be output as not available (i.e. N/A) because the
owner of the sensor is not the BMC. To attempt to bridge sensors
and access sensors not on the BMC, users may wish to try the \fB\-b\fR
or \fB\-\-bridge\-sensors\fR options.
.SH "REPORTING BUGS"
Report bugs to <freeipmi\-users@gnu.org> or <freeipmi\-devel@gnu.org>.
.SH "COPYRIGHT"
Copyright \(co 2003-2012 FreeIPMI Core Team.
.PP
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.
.SH "SEE ALSO"
freeipmi(7), bmc-config(8), bmc-device(8), ipmi-sensors-config(8),
freeipmi_interpret_sensor.conf(5)
.PP
http://www.gnu.org/software/freeipmi/
